# Article
---
This programm is used to add, edit, delete and display articles. The programm can read and write these articles from/to a .csv file.

# Licence
---
Distributed under the Apache Licence Version 2.0.

# Autor
---
* ComradeBlack (https://bitbucket.org/ComradeBlack/)

